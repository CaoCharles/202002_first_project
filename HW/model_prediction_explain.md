# Model Prediction Explain

## 模型預測-變數分析

``` python

model = XGBRegressor(
    max_depth=8,
    n_estimators=1000,
    min_child_weight=300, 
    colsample_bytree=0.8, 
    subsample=0.8, 
    eta=0.3,    
    seed=329)

model.fit(
    X_train, 
    Y_train, 
    eval_metric="rmse", 
    eval_set=[(X_train, Y_train), (X_valid, Y_valid)], 
    verbose=True, 
    early_stopping_rounds = 10)

# set index to ID to avoid droping it later
test  = pd.read_csv('../input/competitive-data-science-predict-future-sales/test.csv').set_index('ID')

# 將預測結果
Y_pred = model.predict(X_valid).clip(0, 20)
Y_test = model.predict(X_test).clip(0, 20)

submission = pd.DataFrame({
    "ID": test.index, 
    "item_cnt_month": Y_test
})
submission.to_csv('xgb_submission.csv', index=False)

# save predictions for an ensemble
pickle.dump(Y_pred, open('xgb_train.pickle', 'wb'))
pickle.dump(Y_test, open('xgb_test.pickle', 'wb'))

# check number
print(X_valid.shape)
print(len(Y_valid_pred))
print(len(Y_valid))

# model.evals_result()['validation_0']['rmse'][-1]
# model.evals_result()['validation_1']['rmse'][-1]

from sklearn import metrics, model_selection

print('R2:',metrics.r2_score(Y_valid, Y_valid_pred))
print('MSE:',metrics.mean_squared_error(Y_valid, Y_valid_pred))
```
## 觀察模型重要特徵

* 列出前五名重要變數
    * date_item_avg_item_cnt_lag_1
    * item_category_id
    * delta_price_lag
    * date_shop_subtype_avg_item_cnt_lag_1
    * modnth

## 針對有興趣討論的變數進行分析

* 實際金額與預測金額差距範圍:(-15.37, 19.98)
* 對差距較大的商品近一步討論

``` python
# 計算實際金額與預測金額的差距
X_valid['item_cnt_month']=Y_valid
X_valid['item_cnt_month_Pred']=Y_valid_pred

X_valid['item_cnt_month_diff']=X_valid['item_cnt_month']-X_valid['item_cnt_month_Pred']
X_valid['item_cnt_month_diff_abs']=abs(X_valid['item_cnt_month']-X_valid['item_cnt_month_Pred'])

pd.set_option('display.max_rows', 1000) # default 60
pd.set_option('display.max_columns', 500) # default 20
pd.set_option('display.max_colwidth', -1) # show all the text strings in the column
pd.set_option('float_format', '{:f}'.format)

X_valid[['item_cnt_month_diff','item_cnt_month_diff_abs']].describe()

```
![](pic/0329_explain_2.png)

## 

``` python
# 指定後續想分析的欄位（避免顯示太多無關的變數欄位）
col_check=['date_block_num','shop_id','item_id','city_code','item_category_id','type_code','subtype_code','month','days'
        ,'item_cnt_month','item_cnt_month_Pred','item_cnt_month_diff','item_cnt_month_diff_abs']

X_valid[col_check].head()

# 挑出模型預測很不好的案例（差異超過75%分位）
col_name='item_cnt_month_diff_abs'
diff_75=X_valid[col_name].describe()['75%']
print(diff_75)
X_valid[col_name+'_over75']=np.where(X_valid[col_name]>diff_75,'75up','normal')

# 確認佔總數之比重
print(X_valid.groupby([col_name+'_over75'])['date_block_num'].count()/X_valid.shape[0])


## 檢查猜測不好的樣本落在哪些地方
', '.join(X_valid.columns)

# 確認模型猜不好的樣本，都散落在哪些 item_category_id
col='item_category_id'
df_tmp=pd.crosstab(X_valid[col],X_valid['item_cnt_month_diff_abs_over75']
           , normalize=True,margins=True)

# 去掉不需要用到的列統計值：All
df_tmp.drop('All',inplace=True)

# 計算猜得準相對猜不準的比重(bad-good ratio)
df_tmp['bg_ratio']=df_tmp['75up']/df_tmp['normal']
display(df_tmp.sort_values('75up',ascending=False).head())

plt.rcParams['font.family'] = ['Droid Sans Fallback']# 用来正常显示中文标签  
plt.rcParams['axes.unicode_minus']=False # 用来正常显示负号

display(df_tmp.sort_values('All',ascending=False)[['75up','normal']].plot.bar(
  stacked=True, figsize=(15,5), title='依照銷售量大到小排列，看75up(猜不准)的分佈'))

display(df_tmp.sort_values('All',ascending=False)[['bg_ratio']].plot.bar(
stacked=True, figsize=(15,5), title='依照銷售量大到小排列，看看bg_ratio的分佈'))

display(df_tmp.sort_values('75up',ascending=False)[['All']].plot.bar(
  stacked=True, figsize=(15,5), title='依照75up(猜不準)大到小排列，看看銷售量的分佈'))

# 確認模型猜不好的樣本，都散落在哪些 item_category_id, type_code, subtype_code
df_tmp=pd.crosstab([X_valid['item_category_id'], X_valid['type_code'], X_valid['subtype_code']],
                   [X_valid['item_cnt_month_diff_abs_over75']], 
                    normalize=True)
display(df_tmp.sort_values('75up',ascending=False).head())

fig, ax = plt.subplots(figsize=(15,15))
sns.heatmap(df_tmp
#             , cmap="Blues"
           , cmap=sns.color_palette("RdBu_r", 7)
           , annot=True #, cbar=False
           ,ax=ax
        )

# 確認模型猜不好的樣本，都散落在哪些 city_code
col='city_code'
df_tmp=pd.crosstab(X_valid[col],X_valid['item_cnt_month_diff_abs_over75']
           , normalize=True,margins=True)
# 去掉不需要用到的列統計值：All
df_tmp.drop('All',inplace=True)

# 計算猜得準相對猜不準的比重(bad-good ratio)
df_tmp['bg_ratio']=df_tmp['75up']/df_tmp['normal']
display(df_tmp.sort_values('75up',ascending=False).head())

plt.rcParams['font.family'] = ['Droid Sans Fallback']# 用来正常显示中文标签  
plt.rcParams['axes.unicode_minus']=False # 用来正常显示负号

display(df_tmp.sort_values('All',ascending=False)[['75up','normal']].plot.bar(
  stacked=True, figsize=(15,5), title='依照銷售量大到小排列，看75up(猜不准)的分佈'))

display(df_tmp.sort_values('All',ascending=False)[['bg_ratio']].plot.bar(
  stacked=True, figsize=(15,5), title='依照銷售量大到小排列，看看bg_ratio的分佈'))

display(df_tmp.sort_values('75up',ascending=False)[['All']].plot.bar(
  stacked=True, figsize=(15,5), title='依照75up(猜不準)大到小排列，看看銷售量的分佈'))

# 確認模型猜不好的樣本，都散落在哪些 city_code, item_category_id, type_code, subtype_code
df_tmp=pd.crosstab([X_valid['item_category_id']
                 ,X_valid['type_code'],X_valid['subtype_code'],X_valid['city_code']]
                 , [X_valid['item_cnt_month_diff_abs_over75']]
                 , normalize=True)
# 只針對75up前三名分析
df_tmp=df_tmp[df_tmp.index.get_level_values('item_category_id').isin(['40','55','37'])]

display(df_tmp.sort_values('75up',ascending=False).head())

fig, ax = plt.subplots(figsize=(15,20))
sns.heatmap(df_tmp
#             , cmap="Blues"
           , cmap=sns.color_palette("RdBu_r", 7)
           , annot=True #, cbar=False
           ,ax=ax
        )

```

## 請回答下列問題

1. 本模型是否會隨著銷售量越低，item_category_id猜不準的比例越高？

* 會，從下圖可以看出，商品銷售量越低，其比例(bg_ratio)有越高的趨勢，表示商品銷售量越低有可能會影響模型預估準確率。

![](pic/0329_diff_2.png)

2. 猜不準的item_category_id前三名為？ 這三名是否因為銷售量較差，所以猜不準？

* 猜不準的item_category_id = 71、79、35，其商品銷售量較差，但猜不準的原因有很多種，還是要看模型其他變數的表現如何在進行判斷

![](pic/0329_bg_ratio.png)

3. 本模型是否會隨著銷售量越低，city_code猜不準的比例越高？

* 不會，從下圖可以觀察出，銷售量較低的城市，其猜不準的比例沒有高於銷售量較高的城市。

![](pic/0329_city_code.png)
