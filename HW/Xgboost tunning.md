# Xgboost parameter tunning

## 競賽內容簡介

## CRSS AC model training for pureCRSS.

FILE STRUCTURE

2020_03_XGB_MODEL
│
├───crss_model_v(x).py
│
├───data
│   └───data.pkl
│
├───model
│   ├───(train : date_block_num < 33)
│   ├───(valid : date_block_num = 33)
│   └───(test : date_block_num = 34)
│
└───result
    ├───(experimental results)
    └───(feature importance figures)


PROGRAM FLOW
│
├───Packages and Defintions
│   ├───Packages
│   ├───Definitions
│   ├───I/O paths
│   └───Column names
│
├───Functions
│   ├───Processing Functions
│   ├───Evaluation Metrics
│   ├───Feature Creation with quick training│  
│   ├───Training Functions w/o CV
│   └───Hyperparameter Tunning
│
├───Modeling
│   └───CV n-folds
│
└───Result
    ├───(experimental results)
    └───(feature importance figures)

    Start Date: 2020.03.12
    Modified Date: 2020.03.12 


``` python

# 設定顯示最大列數與行數
import numpy as np
import pandas as pd
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 100)

# 繪製圖表使用套件
import seaborn as sns
import matplotlib.pyplot as plt
%matplotlib inline

# xgboost模型使用套件
from xgboost.sklearn import XGBRegressor
from xgboost import plot_importance

# 模型訓練分數
from hyperopt import fmin, tpe, hp
from sklearn.metrics import mean_squared_error

# 系統設置使用套件
import time
import sys
import gc
import pickle
sys.version_info

# 模型欄位重要性視覺化
def plot_features(booster, figsize):    
    fig, ax = plt.subplots(1,1,figsize=figsize)
    return plot_importance(booster=booster, ax=ax)

## Data fields 欄位資訊
* ID - an Id that represents a (Shop, Item) tuple within the test set
* shop_id - unique identifier of a shop
* item_id - unique identifier of a product
* item_category_id - unique identifier of item category
* item_cnt_day - number of products sold. You are predicting a monthly amount of this measure
* item_price - current price of an item
* date - date in format dd/mm/yyyy
* date_block_num - a consecutive month number, used for convenience. January 2013 is 0, February 2013 is 1,..., October 2015 is 33
* item_name - name of item
* shop_name - name of shop
* item_category_name - name of item category

``` python
X_train = data[data.date_block_num < 33].drop(['item_cnt_month'], axis=1)
Y_train = data[data.date_block_num < 33]['item_cnt_month']
X_valid = data[data.date_block_num == 33].drop(['item_cnt_month'], axis=1)
Y_valid = data[data.date_block_num == 33]['item_cnt_month']
X_test = data[data.date_block_num == 34].drop(['item_cnt_month'], axis=1)

# 設置機率存儲變數欄位
Result_Dev = np.zeros((len(X_train), 1))
pred_test = np.zeros((len(X_test)))

def objective(params):
    params = {
        'max_depth': int(params['max_depth']),
        'n_estimators': int(params['n_estimators']),
        'learning_rate': params['learning_rate'],
        'min_child_weight': int(params['min_child_weight']),
        'subsample': params['subsample'],
        'colsample_bytree': params['colsample_bytree'],
        'colsample_bylevel': params['colsample_bylevel'],
        'gamma': params['gamma'],
        'alpha': params['alpha'],
        'lambda': params['lambda'],
        'booster': params['booster'],
        'scale_pos_weight': params['scale_pos_weight'],
        'objective': params['objective'],
#         'nthread': params['nthread'],
    }

    d_Train_X = X_train
    d_Train_Y = Y_train
    d_valid_X = X_valid
    d_valid_Y = Y_valid
    
    xgbr = XGBRegressor(**params)

    # 評分標為 mse，驗證集作為 eval_set，並設定 early_stopping_rounds 為 100次
    eval_set = [(d_Train_X, d_Train_Y), (d_valid_X, d_valid_Y)]

    model = xgbr.fit(d_Train_X,
                     d_Train_Y,
                     eval_metric='rmse',
                     eval_set=eval_set,
                     early_stopping_rounds=100,
                     verbose=True)
    
    Pred = model.predict(d_Train_X).clip(0, 20)
    Result_Dev[data.date_block_num < 33] = pd.DataFrame(Pred)
    Pred = model.predict(d_Valid_X).clip(0, 20)
    Result_Dev[data.date_block_num = 33] = pd.DataFrame(Pred)
    pred_test = model.predict_proba(Test_X)[:, 1]

    print(f'best_ntree 為 {model.best_ntree_limit}')
    XGB_MSE = mean_squared_error(Y_valid, Pred)
    XGB_AUC = roc_auc_score(DATA_PLOT['Test_Y'], DATA_PLOT['Pred_Prob'])
    return XGB_MSE

space = {
    'max_depth': hp.quniform('max_depth', 3, 10, 1),
    'n_estimators': hp.quniform('n_estimators', 2500, 5000, 250),
    'learning_rate': hp.quniform('learning_rate', 0.015, 0.025, 0.005),
    'min_child_weight': hp.quniform('min_child_weight', 1, 6, 1),
    'subsample': hp.quniform('subsample', 0.5, 1, 0.05),
    'colsample_bytree': hp.uniform('colsample_bytree', 0.3, 1.0),
    'colsample_bylevel': hp.uniform('colsample_bylevel', 0.8, 1.0),
    'gamma': hp.uniform('gamma', 0.0, 0.5),
    'alpha': hp.uniform('alpha', 0.0, 1.0),
    'lambda': hp.uniform('lambda', 0.0, 1.0),
    'booster': 'gbtree',
    'scale_pos_weight': hp.uniform('scale_pos_weight', 1, 100),
    'objective': 'reg:linear',
#     'nthread': n_jobs,
}

best = fmin(fn=objective, space=space, algo=tpe.suggest, max_evals=10)
```
